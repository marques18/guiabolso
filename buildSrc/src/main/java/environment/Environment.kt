package environment

object Environment {

    const val host = "API_HOST"
    const val key = "API_KEY"

    object Debug {
        const val host = "\"https://api.chucknorris.io/\""
        const val key = "\"xxxxxx\""
    }

    object Release {
        const val host = "\"https://api.chucknorris.io/\""
        const val key = "\"xxxxxx\""
    }
}