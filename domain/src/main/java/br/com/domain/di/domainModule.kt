package br.com.domain.di

import br.com.domain.usecases.GetCategoriesUseCase
import br.com.domain.usecases.GetJokesUseCases
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.module

val useCaseModule = module {
    factory {
        GetCategoriesUseCase(
            repository = get(),
            scheduler = Schedulers.io()
        )
        GetJokesUseCases(
            repository = get(),
            scheduler = Schedulers.io()
        )
    }
}

val domainModule = listOf(useCaseModule)