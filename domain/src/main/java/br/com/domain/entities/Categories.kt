package br.com.domain.entities

data class Categories (
    val categories: List<Category>
)

data class Category (
    val name: String
)