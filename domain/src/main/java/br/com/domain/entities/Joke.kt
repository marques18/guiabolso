package br.com.domain.entities

import java.util.*

data class Joke(
    val categories: List<String>,
    val createdAt: String,
    val updatedAt: String,
    val iconUrl: String,
    val id: String,
    val url: String,
    val value: String
)