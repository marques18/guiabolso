package br.com.domain.repository

import br.com.domain.entities.Category
import io.reactivex.Single

interface CategoriesRepository {
    fun getCategories(forceUpdate: Boolean): Single<List<Category>>
}