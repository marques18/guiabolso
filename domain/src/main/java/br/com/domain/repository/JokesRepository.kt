package br.com.domain.repository

import br.com.domain.entities.Joke
import io.reactivex.Single

interface JokesRepository {
    fun getJoke(joke: String): Single<Joke>
}