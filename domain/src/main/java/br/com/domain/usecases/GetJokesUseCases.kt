package br.com.domain.usecases

import br.com.domain.entities.Joke
import br.com.domain.repository.JokesRepository
import io.reactivex.Scheduler
import io.reactivex.Single

class GetJokesUseCases (
    private val repository: JokesRepository,
    private val scheduler: Scheduler
){
    fun execute(category: String): Single<Joke>{
        return repository.getJoke(category).subscribeOn(scheduler)
    }
}