package br.com.domain.usecases

import br.com.domain.entities.Category
import br.com.domain.repository.CategoriesRepository
import io.reactivex.Scheduler
import io.reactivex.Single

class GetCategoriesUseCase (
    private val repository: CategoriesRepository,
    private val scheduler: Scheduler
){
    fun execute(forceUpdate: Boolean): Single<List<Category>>{
        return repository.getCategories(forceUpdate)
            .subscribeOn(scheduler)
    }
}