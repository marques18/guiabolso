package br.com.data

import androidx.room.Update
import br.com.data.local.source.CategoriesCacheDataSource
import br.com.data.remote.source.RemoteDataSource
import br.com.domain.entities.Category
import br.com.domain.repository.CategoriesRepository
import io.reactivex.Single

class CategoryRepositoryImpl(
    private val categoriesCacheDataSource: CategoriesCacheDataSource,
    private val remoteDataSource: RemoteDataSource
) : CategoriesRepository {
    override fun getCategories(forceUpdate: Boolean): Single<List<Category>> {
        return if (forceUpdate)
            getCategoriesRemote(forceUpdate)
        else categoriesCacheDataSource.getCategories().flatMap {
            when {
                it.isEmpty() -> getCategoriesRemote(false)
                else -> Single.just(it)
            }
        }
    }

    private fun getCategoriesRemote(isUpdate: Boolean): Single<List<Category>> {
        return remoteDataSource.getCategories()
            .flatMap {
                if (isUpdate)
                    categoriesCacheDataSource.updateData(it)
                else
                    categoriesCacheDataSource.insertData(it)
                Single.just(it)
            }
    }
}