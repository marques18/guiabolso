package br.com.data.remote.model

import com.google.gson.annotations.SerializedName
import java.util.*

class JokePayload(
    @SerializedName("categories")
    val categories: List<String>,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("icon_url")
    val iconUrl: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("value")
    val value: String
)