package br.com.data.remote.model

class CategoryPayload(
    val category: List<String>
)