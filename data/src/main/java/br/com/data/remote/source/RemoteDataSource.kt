package br.com.data.remote.source

import br.com.domain.entities.Category
import br.com.domain.entities.Joke
import io.reactivex.Single

interface RemoteDataSource{
    fun getCategories(): Single<List<Category>>
    fun getJoke(category: String): Single<Joke>
}