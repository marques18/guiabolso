package br.com.data.remote.mapper

import br.com.data.remote.model.CategoryPayload
import br.com.domain.entities.Category

object CategoryMapper {

    fun map(payload: Array<String>) = payload.map { map(it) }

    private fun map(payload: String) = Category(
        name = payload
    )
}