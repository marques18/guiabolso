package br.com.data.remote.mapper

import br.com.data.remote.model.JokePayload
import br.com.domain.entities.Joke

object JokeMapper {
    fun map(jokePayload: JokePayload) = Joke(
        url = jokePayload.url,
        id = jokePayload.id,
        categories = jokePayload.categories,
        createdAt = jokePayload.createdAt,
        iconUrl = jokePayload.iconUrl,
        updatedAt = jokePayload.updatedAt,
        value = jokePayload.value
    )
}