package br.com.data.remote.source

import br.com.data.remote.api.ServerAPI
import br.com.data.remote.mapper.CategoryMapper
import br.com.data.remote.mapper.JokeMapper
import br.com.domain.entities.Category
import br.com.domain.entities.Joke
import io.reactivex.Single

class RemoteDataSourceImpl(
    private val serverAPI: ServerAPI
): RemoteDataSource {
    override fun getCategories(): Single<List<Category>> {
        return serverAPI.fetchCategories().map {
            CategoryMapper.map(it)
        }
    }

    override fun getJoke(category: String): Single<Joke> {
        return serverAPI.fetchJoke(category).map {
            JokeMapper.map(it)
        }
    }
}