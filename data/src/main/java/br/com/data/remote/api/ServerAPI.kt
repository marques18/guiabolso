package br.com.data.remote.api

import br.com.data.remote.model.CategoryPayload
import br.com.data.remote.model.JokePayload
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface ServerAPI{
    @GET("jokes/categories")
    fun fetchCategories(): Single<Array<String>>

    @GET("jokes/random")
    fun fetchJoke(@Query("category") category: String): Single<JokePayload>
}