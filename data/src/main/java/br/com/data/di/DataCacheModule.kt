package br.com.data.di

import br.com.data.local.database.CategoriesDB
import br.com.data.local.source.CategoriesCacheDataSource
import br.com.data.local.source.CategoriesCacheDataSourceImp
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val cacheDataModule = module {
    single { CategoriesDB.createDB(androidContext()) }
    factory<CategoriesCacheDataSource> { CategoriesCacheDataSourceImp(categoriesDb = get()) }
}