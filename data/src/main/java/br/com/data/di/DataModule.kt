package br.com.data.di

import br.com.data.CategoryRepositoryImpl
import br.com.data.JokeRepositoryImpl
import br.com.domain.repository.CategoriesRepository
import br.com.domain.repository.JokesRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<CategoriesRepository> {
        CategoryRepositoryImpl(
            categoriesCacheDataSource = get(),
            remoteDataSource = get()
        )
    }
    factory<JokesRepository> {
        JokeRepositoryImpl(
            remoteDataSource = get()
        )
    }
}

val dataModules = listOf(remoteDataSourceModule, repositoryModule, cacheDataModule)