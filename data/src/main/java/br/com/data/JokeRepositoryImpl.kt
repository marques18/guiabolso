package br.com.data

import br.com.data.remote.source.RemoteDataSource
import br.com.domain.entities.Joke
import br.com.domain.repository.JokesRepository
import io.reactivex.Single

class JokeRepositoryImpl(
    private val remoteDataSource: RemoteDataSource
) : JokesRepository {

    override fun getJoke(joke: String): Single<Joke> {
        return remoteDataSource.getJoke(joke)
            .flatMap {
                Single.just(it)
            }
    }

}