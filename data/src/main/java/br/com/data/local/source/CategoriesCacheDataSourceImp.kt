package br.com.data.local.source

import br.com.data.local.database.CategoriesDB
import br.com.data.local.mapper.CategoriesCacheMapper
import br.com.domain.entities.Category
import io.reactivex.Single

class CategoriesCacheDataSourceImp(
    private val categoriesDb: CategoriesDB
): CategoriesCacheDataSource {

    private val categoriesDao by lazy {
        categoriesDb.categoriesDao()
    }

    override fun getCategories(): Single<List<Category>> {
        return categoriesDao.getCategories().map { CategoriesCacheMapper.map(it) }
    }

    override fun insertData(list: List<Category>) {
        categoriesDao.insertAll(CategoriesCacheMapper.mapCategoriesToCache(list))
    }

    override fun updateData(list: List<Category>) {
        categoriesDao.updateData(CategoriesCacheMapper.mapCategoriesToCache(list))
    }

}