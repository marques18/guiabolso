package br.com.data.local.mapper

import br.com.data.local.model.CategoriesCache
import br.com.domain.entities.Category

object CategoriesCacheMapper{

    fun map(cacheData: List<CategoriesCache>) = cacheData.map { map(it) }

    private fun map(cacheData: CategoriesCache) = Category(
        name = cacheData.name
    )

    fun mapCategoriesToCache(categories: List<Category>) = categories.map { map(it) }

    private fun map(data: Category) = CategoriesCache(
        name = data.name
    )
}