package br.com.data.local.source

import br.com.domain.entities.Category
import io.reactivex.Single

interface CategoriesCacheDataSource {
    fun getCategories(): Single<List<Category>>

    fun insertData(list: List<Category>)
    fun updateData(list: List<Category>)
}