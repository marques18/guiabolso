package br.com.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.data.local.model.CategoriesCache

@Database(version = 1, exportSchema = false, entities = [CategoriesCache::class])
abstract class CategoriesDB: RoomDatabase(){
    abstract fun categoriesDao(): CategoriesDao

    companion object {
        fun createDB(context: Context) : CategoriesDB{
            return Room
                .databaseBuilder(context, CategoriesDB::class.java, "categories.db")
                .build()
        }
    }
}