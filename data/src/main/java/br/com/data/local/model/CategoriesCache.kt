package br.com.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categories")
data class CategoriesCache(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var name: String
)