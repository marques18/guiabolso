package br.com.data.local.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import br.com.data.local.model.CategoriesCache
import io.reactivex.Single

@Dao
interface CategoriesDao {

    @Query("SELECT * FROM categories")
    fun getCategories(): Single<List<CategoriesCache>>

    @Transaction
    fun updateData(categories: List<CategoriesCache>){
        deleteAll()
        insertAll(categories)
    }

    @Insert
    fun insertAll(categories: List<CategoriesCache>)

    @Query("DELETE FROM categories")
    fun deleteAll()
}