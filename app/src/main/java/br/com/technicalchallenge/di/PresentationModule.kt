package br.com.technicalchallenge.di

import br.com.technicalchallenge.feature.home_category.CategoryFragment
import br.com.technicalchallenge.feature.home_category.CategoryListAdapter
import br.com.technicalchallenge.feature.home_category.CategoryViewModel
import br.com.technicalchallenge.feature.joke.JokeFragment
import br.com.technicalchallenge.feature.joke.JokeViewModel
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel {
        CategoryViewModel(get())
    }
    viewModel {
        JokeViewModel(get())
    }
    single { CategoryListAdapter() }
    fragment {
        CategoryFragment(get())
    }
    fragment{
        JokeFragment(get())
    }
}