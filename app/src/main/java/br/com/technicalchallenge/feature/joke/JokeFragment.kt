package br.com.technicalchallenge.feature.joke

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import br.com.technicalchallenge.R
import br.com.technicalchallenge.extensions.onStateChange
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_joke.*


class JokeFragment(
    private val viewModel: JokeViewModel
) : Fragment(R.layout.fragment_joke) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back.setOnClickListener {
            activity?.finish()
        }

        arguments?.apply {
            val category = this.getString("CATEGORY","")
            viewModel.getJoke(category)

            renew.setOnClickListener {
                viewModel.getJoke(category)
            }
        }

        setStateChangeListener()
    }

    private fun setStateChangeListener() = onStateChange(viewModel) {
        mcv.isVisible = it.isItemVisible
        progressBar.isVisible = it.isProgressBarVisible
        value.text = it.item?.value
        categories.text = "Categories: ${it.item?.categories}"
        url.text = it.item?.url
        url.setOnClickListener {view ->
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(it.item?.url)
            startActivity(i)
        }

        Glide.with(this).load(it.item?.iconUrl).into(chuck_norris)
    }

}