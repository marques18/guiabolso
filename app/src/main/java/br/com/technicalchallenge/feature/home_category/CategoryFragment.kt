package br.com.technicalchallenge.feature.home_category

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.technicalchallenge.R
import br.com.technicalchallenge.extensions.onStateChange
import kotlinx.android.synthetic.main.fragment_category.*
import org.koin.android.ext.android.inject

class CategoryFragment(
    private val viewModel: CategoryViewModel
) : Fragment(R.layout.fragment_category) {

    private val adapter : CategoryListAdapter by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupRecyclerView()
        viewModel.getCategories()
        setStateChangeListener()
    }

    private fun setupRecyclerView() {
        list.apply {
            val orientation = RecyclerView.VERTICAL

            adapter = this@CategoryFragment.adapter
            layoutManager = GridLayoutManager(context, 2, orientation, false)
        }
    }

    private fun setStateChangeListener() = onStateChange(viewModel) {
        adapter.elements = it.listItems
        progressBarRecente.isVisible = it.isProgressBarVisible
        list.isVisible = it.isListVisible
    }
}