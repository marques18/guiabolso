package br.com.technicalchallenge.feature.joke

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.technicalchallenge.R
import kotlinx.android.synthetic.main.activity.*
import org.koin.androidx.fragment.android.setupKoinFragmentFactory

class JokeActivity: AppCompatActivity(R.layout.activity) {

    companion object {
        const val _category = "CATEGORY"
        fun launchIntent(context: Context, category: String): Intent {
            return Intent(context, JokeActivity::class.java).putExtra(_category, category)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setupKoinFragmentFactory()
        super.onCreate(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        supportFragmentManager.beginTransaction()
            .add(content.id, JokeFragment::class.java, intent.extras, null)
            .commit()
    }
}
