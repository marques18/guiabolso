package br.com.technicalchallenge.feature.home_category

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.domain.entities.Category
import br.com.technicalchallenge.R

class CategoryListAdapter : RecyclerView.Adapter<CategoryListItemViewHolder>() {

    private lateinit var context: Context
    var elements: List<CategoryState> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryListItemViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context)
            .inflate(R.layout.list_item_category, parent, false)

        return CategoryListItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryListItemViewHolder, position: Int) {
        holder.bind(elements[position], context)
    }

    override fun getItemCount() = elements.size

}