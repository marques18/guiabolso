package br.com.technicalchallenge.feature.home_category

data class CategoryState(
    val name: String
)