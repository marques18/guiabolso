package br.com.technicalchallenge.feature.home_category

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.domain.entities.Category
import br.com.technicalchallenge.feature.joke.JokeActivity
import kotlinx.android.synthetic.main.list_item_category.view.*

class CategoryListItemViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {
    fun bind(category: CategoryState, context: Context) {
        itemView.category.text = category.name

        itemView.setOnClickListener {
            context.startActivity(JokeActivity.launchIntent(context, category.name))
        }
    }
}
