package br.com.technicalchallenge.feature.home_category

import android.util.Log
import br.com.domain.repository.CategoriesRepository
import br.com.technicalchallenge.extensions.UIAction
import br.com.technicalchallenge.extensions.UIState
import br.com.technicalchallenge.feature.viewmodel.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class CategoryViewModel(
    val repository: CategoriesRepository
) : ViewModel<CategoryViewState, CategoryViewAction>(CategoryViewState.initialState) {

    fun getCategories() {
        repository.getCategories(false)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { setState { CategoryViewState(isProgressBarVisible = true) } }
            .map {
                it.map { category ->
                    CategoryState(
                        name = category.name
                    )
                }
            }
            .subscribeBy (
                onError = {
                    Log.i("e", it.toString())
                    setState { CategoryViewState(isError = true) }
                },
                onSuccess = { categories ->
                    setState { CategoryViewState(isListVisible = true, listItems = categories) }
                }
            )
            .handleDisposable()
    }
}

sealed class CategoryViewAction : UIAction
data class CategoryViewState(
    val isProgressBarVisible: Boolean = false,
    val isError: Boolean = false,
    val isListVisible: Boolean = false,
    val listItems: List<CategoryState> = emptyList()
) : UIState {

    companion object {
        val initialState
            get() = CategoryViewState()
    }
}