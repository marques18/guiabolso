package br.com.technicalchallenge.feature.home_category

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.technicalchallenge.R
import kotlinx.android.synthetic.main.activity.*
import org.koin.androidx.fragment.android.setupKoinFragmentFactory

class HomeCategoryActivity : AppCompatActivity(R.layout.activity) {

    override fun onCreate(savedInstanceState: Bundle?) {
        setupKoinFragmentFactory()
        super.onCreate(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        supportFragmentManager.beginTransaction()
            .add(content.id, CategoryFragment::class.java, intent.extras, null)
            .commit()
    }
}
