package br.com.technicalchallenge.feature.joke

import java.util.*

data class JokeState(
    val categories: List<String>,
    val createdAt: String,
    val updatedAt: String,
    val iconUrl: String,
    val id: String,
    val url: String,
    val value: String
)