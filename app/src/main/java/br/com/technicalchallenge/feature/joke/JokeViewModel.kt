package br.com.technicalchallenge.feature.joke

import android.util.Log
import br.com.domain.repository.JokesRepository
import br.com.technicalchallenge.extensions.UIAction
import br.com.technicalchallenge.extensions.UIState
import br.com.technicalchallenge.feature.viewmodel.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class JokeViewModel(
    private val repository: JokesRepository
) : ViewModel<JokeViewState, JokeViewAction>(
    JokeViewState.initialState
) {
    fun getJoke(category: String){
        repository.getJoke(category)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { setState { JokeViewState(isProgressBarVisible = true) } }
            .map {
                JokeState(
                    value = it.value,
                    updatedAt = it.updatedAt,
                    iconUrl = it.iconUrl,
                    createdAt = it.createdAt,
                    categories = it.categories,
                    id = it.id,
                    url = it.url
                )
            }
            .subscribeBy (
                onError = {
                    Log.i("e", it.toString())
                    setState { JokeViewState(isError = true) }
                },
                onSuccess = { joke ->
                    Log.i("e", joke.url)
                    setState { JokeViewState(isItemVisible = true, item = joke) }
                }
            )
            .handleDisposable()
    }
}

sealed class JokeViewAction : UIAction
data class JokeViewState(
    val isProgressBarVisible: Boolean = false,
    val isError: Boolean = false,
    val isItemVisible: Boolean = false,
    val item: JokeState? = null
) : UIState {

    companion object {
        val initialState
            get() = JokeViewState()
    }
}